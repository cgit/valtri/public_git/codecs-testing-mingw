#! /bin/bash -xe

#
# dnf install gsm-devel mingw32-gcc mingw64-gcc
# dnf install mingw32-gsm mingw64-gsm --enablerepo=updates-testing
#

rm -f gsmdec* gsmenc* jam*.wave jam*.gsm

sh -x `dirname $0`/gsm.c

dd if=/dev/urandom bs=320 count=10240 > jam.wave

./gsmenc-linux < jam.wave > jam-linux.gsm
./gsmdec-linux < jam-linux.gsm > jam-decoded-linux.wave

export WINEPREFIX=$HOME/.wine32
./gsmenc-win32.exe < jam.wave > jam-win32.gsm
./gsmdec-win32.exe < jam-linux.gsm > jam-decoded-win32.wave

export WINEPREFIX=$HOME/.wine64
./gsmenc-win64.exe < jam.wave > jam-win64.gsm
./gsmdec-win64.exe < jam-linux.gsm > jam-decoded-win64.wave

md5sum jam*.wave jam*.gsm
