#define CC /*
gcc -W -Wall -g -O2 $0 -o gsmdec-linux -lgsm || exit $?
i686-w64-mingw32-gcc -W -Wall -g -O2 $0 -o gsmdec-win32.exe -lgsm || exit $?
x86_64-w64-mingw32-gcc -W -Wall -g -O2 $0 -o gsmdec-win64.exe -lgsm || exit $?
gcc -W -Wall -g -O2 -DENCODE $0 -o gsmenc-linux -lgsm || exit $?
i686-w64-mingw32-gcc -W -Wall -g -O2 -DENCODE $0 -o gsmenc-win32.exe -lgsm || exit $?
x86_64-w64-mingw32-gcc -W -Wall -g -O2 -DENCODE $0 -o gsmenc-win64.exe -lgsm || exit $?
exit 0
*/

#include <unistd.h>
#include <gsm.h>
#ifdef WIN32
#include <fcntl.h>
#endif

int main() {
	gsm g;
	gsm_signal wave[160];
	gsm_frame data;
	ssize_t len;

#ifdef WIN32
	setmode(0, _O_BINARY);
	setmode(1, _O_BINARY);
#endif
	g = gsm_create();
#ifdef ENCODE
	while ((len = read(0, wave, sizeof wave)) != 0) {
		if (len != sizeof wave) return 1;
		gsm_encode(g, wave, data);
		if (write(1, (char *)data, sizeof data) != sizeof data) return 3;
	}
#else
	while ((len = read(0, (char *)data, sizeof data)) != 0) {
		if (len != sizeof data) return 1;
		if (gsm_decode(g, data, wave) < 0) return 2;
		if (write(1, wave, sizeof wave) != sizeof wave) return 3;
	}
#endif
	gsm_destroy(g);

	return 0;
}
