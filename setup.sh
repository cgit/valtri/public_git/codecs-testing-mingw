#! /bin/sh -xe

rm -rf $HOME/.wine32 $HOME/.wine64

(export WINEPREFIX=$HOME/.wine32
echo | wine cmd
sleep 10
sed -i 's/^\("PATH"=.*\)"\s*$/\1;Z:\\\\usr\\\\i686-w64-mingw32\\\\sys-root\\\\mingw\\\\bin"/' $WINEPREFIX/system.reg)&

(export WINEPREFIX=$HOME/.wine64
echo | wine cmd
sleep 10
sed -i 's/^\("PATH"=.*\)"\s*$/\1;Z:\\\\usr\\\\x86_64-w64-mingw32\\\\sys-root\\\\mingw\\\\bin"/' $WINEPREFIX/system.reg)&

wait
