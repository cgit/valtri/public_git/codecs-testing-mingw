#! /bin/bash -xe

w32="/usr/i686-w64-mingw32/sys-root/mingw/bin"
w64="/usr/x86_64-w64-mingw32/sys-root/mingw/bin"

if [ ! -d samples ]; then
	echo "samples directory doesn't exists"
	exit 1
fi

pushd samples

for sample in *.ogg *.ogv; do
	echo "$sample"

	theora_dump_video < $sample | md5sum

	export WINEPREFIX=$HOME/.wine32
	$w32/theora_dump_video.exe < $sample | md5sum

	export WINEPREFIX=$HOME/.wine64
	$w64/theora_dump_video.exe < $sample | md5sum

	echo
done

popd
