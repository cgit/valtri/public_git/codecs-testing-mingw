package Combiner;

use strict;
use warnings;
use Exporter;

our @ISA = qw(Exporter);
our @EXPORT_OK = qw(nop init_w32 init_w64 parallel parallel_w32 parallel_w64 newline end_parallel combiner combiners w32);
our @EXPORT = qw(nop init_w32 init_w64 parallel parallel_w32 parallel_w64 newline end_parallel combiner combiners w66);

our $w32 = '/usr/i686-w64-mingw32/sys-root/mingw/bin/';
our $w64 = '/usr/x86_64-w64-mingw32/sys-root/mingw/bin/';


sub nop() {
}


sub init_w32() {
	print "export WINEPREFIX=\$HOME/.wine32\n";
}


sub init_w64() {
	print "export WINEPREFIX=\$HOME/.wine64\n";
}


sub parallel() {
	print "parallel -v --halt 1 --colsep ' ' <<EOF\n";
}


sub parallel_w32() {
	init_w32();
	parallel();
}


sub parallel_w64() {
	init_w64();
	parallel();
}


sub newline() {
	print "\n";
}


sub end_parallel() {
	print "EOF\n";
	print "\n";
}


sub combiner($$) {
	my ($callback, $comb) = @_;
	my (@s, @n, $i, @comb, @single);
	my ($j, $opts, $name, $overflow, $opname);

	@comb = @$comb;

	# initial state
	foreach $i (0..$#comb) {
		@single = @{$comb[$i]};
		$s[$i] = 0;
		$n[$i] = $#single;
	}

	$overflow = 0;
	while (!$overflow) {
		# actual combination of arguments in @s
		$opts = "";
		$name = "";
		foreach $i (0..$#comb) {
			$opts .= " " if ($opts and $comb[$i][$s[$i]]);
			$opts .= $comb[$i][$s[$i]];

			$opname = $comb[$i][$s[$i]];
			$opname =~ s/^-*//;
			$opname =~ s/ //g;
			$name .= "-" if ($name and $opname);
			$name .= $opname;
		}
		$callback->($name ? $name : 'default', $opts);

		# next combination
		$overflow = 1;
		$i = 0;
		while ($overflow and ($i <= $#comb)) {
			$s[$i]++;
			if ($s[$i] <= $n[$i]) {
				$overflow = 0;
			} else {
				$s[$i] = 0;
				$i++;
			}
		}
	}
}


sub combiners($$$$) {
	my ($inits, $ends, $callbacks, $combs) = @_;
	my ($i, $j, @inits, @ends, @combs, @callbacks);
	@inits = @$inits;
	@ends = @$ends;
	@combs = @$combs;
	@callbacks = @$callbacks;

	foreach $j (0..$#callbacks) {
		print "date\n";
		$inits[$j]->();
		foreach $i (0..$#combs) {
			combiner($callbacks[$j], \@{$combs[$i]});
		}
		$ends[$j]->();
	}
}


1;
