#! /usr/bin/perl -w

use strict;
use warnings;
use Combiner;

my $w32 = $Combiner::w32;
my $w64 = $Combiner::w64;

my ($cmd, $dir);

my @combs = (
	[
		["-f", "-h", ""],
	],
	[
		["-b24", "-b4812", "-b9600"],
		["-c"],
		["-cc", ""],
		["-f", "-h", ""],
	],
	[
		["-b2418"],
		["-c"],
		["-p", ""],
		["--use-dns", ""],
	],
	[
		["-x2", "-x4", "-x6"],
	]
	);

my @inits = (\&nop, \&init_w32, \&init_w64);
my @ends = (\&newline, \&newline, \&newline);
my @inits_parallel = (\&parallel, \&parallel_w32, \&parallel_w64);
my @ends_parallel = (\&end_parallel, \&end_parallel, \&end_parallel);

my @callbacks = (
	sub($$) {
		my ($name, $opts) = @_;

		print "wavpack $opts 1.wav -o linux/$name.wv\n";
	},
	sub($$) {
		my ($name, $opts) = @_;

		print "${w32}wavpack.exe $opts 1.wav -o win32/$name.wv\n";
	},
	sub($$) {
		my ($name, $opts) = @_;

		print "${w64}wavpack.exe $opts 1.wav -o win64/$name.wv\n";
	}
);

my @callbacks_dec = (
	sub($$) {
		my ($name, $opts) = @_;

		print "wvunpack linux/$name.wv -o linux/$name.wav\n";
	},
	sub($$) {
		my ($name, $opts) = @_;

		print "${w32}wvunpack.exe win32/$name.wv -o win32/$name.wav\n";
	},
	sub($$) {
		my ($name, $opts) = @_;

		print "${w64}wvunpack.exe win64/$name.wv -o win64/$name.wav\n";
	},
);


print qq[#! /bin/sh -ex
rm -rf linux win32 win64
mkdir linux win32 win64

];

if ($ARGV[0] eq '--parallel') {
	combiners(\@inits_parallel, \@ends_parallel, \@callbacks, \@combs);
	combiners(\@inits_parallel, \@ends_parallel, \@callbacks_dec, \@combs);
} else {
	combiners(\@inits, \@ends, \@callbacks, \@combs);
	combiners(\@inits, \@ends, \@callbacks_dec, \@combs);
}
