#! /usr/bin/perl -w

use strict;
use warnings;
use Combiner;

my $w32 = $Combiner::w32;
my $w64 = $Combiner::w64;

my ($cmd, $dir);

my @combs = (
	[
		["--narrowband", "--wideband", "--ultra-wideband"],
	], [
		["--quality 0", "--quality 5", "--quality 10", ""],
		["--vbr", ""],
		["--comp 0", "--comp 10", ""]
	]
);

my @inits = (\&nop, \&init_w64);
my @ends = (\&newline, \&newline);
my @inits_parallel = (\&parallel, \&parallel_w64);
my @ends_parallel = (\&end_parallel, \&end_parallel);
my @callbacks = (
	sub($$) {
		my ($name, $opts) = @_;

		print "speexenc $opts 1.wav linux/$name.spx\n";
	},
	sub($$) {
		my ($name, $opts) = @_;

		print "${w64}speexenc.exe $opts 1.wav win64/$name.spx\n";
	}
);

my @inits_dec = (\&nop, \&init_w64, \&init_w64);
my @ends_dec = (\&newline, \&newline, \&newline);
my @inits_dec_parallel = (\&parallel, \&parallel_w64, \&parallel_w64);
my @ends_dec_parallel = (\&end_parallel, \&end_parallel, \&end_parallel);
my @callbacks_dec = (
	sub($$) {
		my ($name, $opts) = @_;

		print "speexdec linux/$name.spx linux/$name.wav\n";
	},
	sub($$) {
		my ($name, $opts) = @_;

		print "${w64}speexdec.exe win64/$name.spx win64/$name.wav\n";
	},
	sub($$) {
		my ($name, $opts) = @_;

		print "${w64}speexdec.exe linux/$name.spx linux-win64/$name.wav\n";
	}
);


print qq[#! /bin/sh -ex
rm -rf linux win32 win64 linux-win32 linux-win64
mkdir linux win64 linux-win64

];

if ($ARGV[0] eq '--parallel') {
	combiners(\@inits_parallel, \@ends_parallel, \@callbacks, \@combs);
	combiners(\@inits_dec_parallel, \@ends_dec_parallel, \@callbacks_dec, \@combs);
} else {
	combiners(\@inits, \@ends, \@callbacks, \@combs);
	combiners(\@inits_dec, \@ends_dec, \@callbacks_dec, \@combs);
}
