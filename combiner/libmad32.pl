#! /usr/bin/perl -w

use strict;
use warnings;
use Combiner;

my ($cmd, $dir);

my @combs = (
	[
		["-b 8", "-b 16", "-b 24", "-b 32", ""],
		["-R 11025", "-R 22050", "-R 44100", ""],
	],
	[
		["-d", "--downsample", "-d --downsample"],
	],
	[
		["-1", "-2", "-m", "-S"],
	],
);

my @inits = (\&nop, \&init_w32);
my @ends = (\&newline, \&newline);
my @inits_parallel = (\&parallel, \&parallel_w32);
my @ends_parallel = (\&end_parallel, \&end_parallel);

my @callbacks = (
	sub($$) {
		my ($name, $opts) = @_;

		print "madplay $opts -o wave:linux32/$name.wav 1.mp3\n";
	},
	sub($$) {
		my ($name, $opts) = @_;

		print "build_win32/madplay.exe $opts -o wave:win32/$name.wav 1.mp3\n";
	}
);

print qq[#! /bin/sh -ex
rm -rf linux32 win32
mkdir linux32 win32

];

if ($ARGV[0] eq '--parallel') {
	combiners(\@inits_parallel, \@ends_parallel, \@callbacks, \@combs);
} else {
	combiners(\@inits, \@ends, \@callbacks, \@combs);
}

print qq[
pushd linux32
for w in *.wav; do
	md5sum \$w ../win32/\$w
	cmp \$w ../win32/\$w
	echo
done
popd
];
