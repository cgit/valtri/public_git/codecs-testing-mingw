#! /usr/bin/perl -w

use strict;
use warnings;
our @EXPORT_OK = qw(combiner);
use Combiner;

my $w32 = $Combiner::w32;
my $w64 = $Combiner::w64;

my ($cmd, $dir);

my @combs = (
	[
		["--fast", "--compression-level-5", ""],
		["--lax", ""]
	],
	[
		["", "--compression-level-8"],
		["--qlp-coeff-precision-search"]
	],
	[
		["--compression-level-1", "--compression-level-2", "--compression-level-3", "--compression-level-4", "--compression-level-6", "--compression-level-7", "--compression-level-8"],
	],
	[
		["-l 0 -b 1152 -r 3", "-b 1152 -r 3", "-l 0 -r 3", "-l 0 -b 1152", "-l 0", "-b 1152", "-r 3"],
	],
	[
		["-l 1", "-l 2"]
	]
	);

my @inits = (\&nop, \&init_w32, \&init_w64);
my @ends = (\&newline, \&newline, \&newline);
my @inits_parallel = (\&parallel, \&parallel_w32, \&parallel_w64);
my @ends_parallel = (\&end_parallel, \&end_parallel, \&end_parallel);

my @callbacks = (
	sub($$) {
		my ($name, $opts) = @_;

		print "flac $opts -o linux/$name.flac 1.wav\n";
	},
	sub($$) {
		my ($name, $opts) = @_;

		print "${w32}flac.exe $opts -o win32/$name.flac 1.wav\n";
	},
	sub($$) {
		my ($name, $opts) = @_;

		print "${w64}flac.exe $opts -o win64/$name.flac 1.wav\n";
	}
);

my @callbacks_dec = (
	sub($$) {
		my ($name, $opts) = @_;

		print "flac -d -o linux/$name.wav linux/$name.flac\n";
	},
	sub($$) {
		my ($name, $opts) = @_;

		print "${w32}flac.exe -d -o win32/$name.wav win32/$name.flac\n";
	},
	sub($$) {
		my ($name, $opts) = @_;

		print "${w64}flac.exe -d -o win64/$name.wav win64/$name.flac\n";
	},
#	sub($$) {
#		my ($name, $opts) = @_;
#
#		print "${w32}flac.exe -d -o linux-win32/$name.wav linux/$name.flac\n";
#	},
#	sub($$) {
#		my ($name, $opts) = @_;
#
#		print "${w64}flac.exe -d -o linux-win64/$name.wav linux/$name.flac\n";
#	}
);


print qq[#! /bin/sh -ex
rm -rf linux win32 win64 linux-win32 linux-win64
mkdir linux win32 win64
# linux-win32 linux-win64

];

if ($ARGV[0] eq '--parallel') {
	combiners(\@inits_parallel, \@ends_parallel, \@callbacks, \@combs);
	combiners(\@inits_parallel, \@ends_parallel, \@callbacks_dec, \@combs);
} else {
	combiners(\@inits, \@ends, \@callbacks, \@combs);
	combiners(\@inits, \@ends, \@callbacks_dec, \@combs);
}
