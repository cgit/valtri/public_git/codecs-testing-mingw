TARGETS=flac libmad libmad32 speex wavpack

all: $(TARGETS)

clean:
	for t in $(TARGETS); do rm -fv $${t}/$${t}.sh $${t}/$${t}-parallel.sh; done

$(TARGETS):
	PERLLIB=$$PRERLLIB:combiner combiner/$@.pl > $@/$@.sh
	chmod +x $@/$@.sh
	PERLLIB=$$PRERLLIB:combiner combiner/$@.pl --parallel > $@/$@-parallel.sh
	chmod +x $@/$@-parallel.sh

.PHONY: $(TARGETS)
